<?php

    include('session_init.php');
    if (((empty($_SESSION['username_link']) || !isset($_SESSION['username_link']))
            && basename($_SERVER['SCRIPT_FILENAME']) != 'login.php')) {
        header("location:login.php");
    } 
    if((isset($_SESSION['username_link']) && ($_SESSION['usergroup_link'] != 1)) && (isset($_SESSION['username_link']) && $_SESSION['userdpto_link'] != 7)) {
        header("location:login.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Pedidos Magento</title>

    <meta charset="utf-8">
     
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- jquery -->
    <link href="vendor/bootstrap/css/jquery-ui.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/jquery-ui.theme.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="vendor/bootstrap/css/pedidos_magento.css" rel="stylesheet">
    <!-- Sign in bootstrap -->
    <link href="vendor/bootstrap/css/signin.css" rel="stylesheet">
    <!-- Datatables -->
    <link href="vendor/bootstrap/css/dataTables.responsive.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/jquery.dataTables_themeroller.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap-select.css" rel="stylesheet">
    <!-- jQuery -->
    <script type="text/javascript" charset="utf8" src="vendor/bootstrap/jquery/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="vendor/bootstrap/js/jquery-ui.min.js"></script>
    <!-- Bootstrap -->
    <script  type="text/javascript" charset="utf8" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" src="vendor/bootstrap/jquery/dataTables.responsive.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/jquery/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/jquery/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/jquery/dataTables.buttons.min.js"></script>
    <!-- Bootbox -->
    <script type="text/javascript" src="vendor/bootstrap/jquery/bootbox.min.js"></script>
    <!-- Customized js -->
    <script type="text/javascript" charset="utf8" src="vendor/bootstrap/js/pedidos_magento.js"></script>
    <script type="text/javascript" charset="utf8" src="vendor/bootstrap/js/bootstrap-select.min.js"></script>
    <!-- Elegant TextArea -->
    <!--<script src="vendor/bootstrap/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>-->
</head>
<body>
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../link/index.php"><img src='images/logo-se.png' alt='SE Logo'></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <?php if($_SESSION['userdpto_link'] == 4) { ?>
          <ul class="nav navbar-nav">
            <li class="nav-item">
                <a class="navbar-brand" href="consulta.php" target="_blank"><font color="<?php if($fich=="consulta.php"){ echo 'red'; } ?>">Tabla de Consulta</font></a>
            </li>
          </ul>
          <?php } ?>
          <ul class="navbar-nav ml-auto">
            <?php
            if(basename($_SERVER['SCRIPT_FILENAME']) == 'login.php') { ?>
                <li class="nav-item">
                  <a class="nav-link" href="login.php">Acceder</a>
                </li>
            <?php
            }
            else { ?>
                <li class="nav-item">
                  <a class="nav-link" href=""><?php echo $_SESSION['username_link']; ?></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="logout.php">Salir</a>
                </li>
            <?php
            }
            ?>
          </ul>
        </div>
    </nav>