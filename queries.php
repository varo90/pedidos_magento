<?php

class queries {
    
    static function get_pedidos_magento() {
        $sql = "SELECT sfo.entity_id,increment_id, created_at, order_currency_code,base_grand_total,
                 CASE
                    WHEN method ='realexredirect' AND order_currency_code='GBP' THEN 'Web Tarjeta de credito LIBRAS'
                    WHEN method ='realexredirect' AND order_currency_code='EUR' THEN 'Web Tarjeta crédito'
                    WHEN method ='paypal_express' AND order_currency_code='GBP' THEN 'Web Paypal GBP'
                    WHEN method ='paypal_express' AND order_currency_code='EUR' THEN 'Web Paypal'
                    WHEN method ='banktransfer' AND order_currency_code='GBP'THEN 'Web Transferencia'
                    WHEN method ='banktransfer' AND order_currency_code='EUR'THEN 'Web Transferencia'
                 ELSE '' END AS method from sales_flat_order sfo 
                JOIN sales_flat_order_payment sfop on sfop.entity_id=sfo.entity_id and method<>'ereserve'
                WHERE state<>'canceled' and state<>'new' AND created_at BETWEEN ? AND ?
                order by created_at asc";
        return $sql;
    }

    static function set_queries() {
        $sql = "BEGIN TRANSACTION;UPDATE SBO_EULALIA.dbo.[@GSP_TPVCAP] SET U_GSP_COMENT=? WHERE Code=?;COMMIT;";
        // $sql = "SELECT ?,? FROM SBO_EULALIA.dbo.[@GSP_TPVCAP]";
        return $sql;
    }

    static function get_pedidos_sap() {
        $sql = "SELECT gtc.Code, U_GSP_CACLIE,U_GSP_CADATA,U_GSP_CANUME,op.Descript,U_GSP_COMENT,U_GSP_CATOTA
                FROM [dbo].[@gsp_tpvcap] gtc WITH (NOLOCK) 
                LEFT JOIN dbo.OPYM op WITH (NOLOCK) on op.PayMethCod=gtc.U_GSP_CAFPAG
                WHERE U_GSP_CABOTI='02' AND U_GSP_CACLIE<>'v34090' AND U_GSP_CACLIE<>'v34092' AND U_GSP_CACLIE<>'34000' AND U_GSP_CAFPAG<>'ERESERVE_WEB' AND U_GSP_CAESTA='X' AND U_GSP_CADATA BETWEEN ? AND ?";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
