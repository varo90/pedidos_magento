<?php

    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
    include('db_connections.php');
    include('queries.php');

    $db_my = new db('ms_edit');

    $query = $db_my->conn->prepare(queries::set_queries());
    $query->execute([$_POST['comentario'],$_POST['id']]);

    unset($db_my);

    // echo queries::set_queries() . ' - ' . $_POST['id'] . ' - ' . $_POST['comentario'];
