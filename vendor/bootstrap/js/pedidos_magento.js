function reload_table() {
    $('#data-info-2').dataTable().fnDestroy();
    show_table();
}

function comentar(id) {
    var comentario = $('#comentario__'+id).val();
    var html = $('#save__'+id).html();
    $.ajax({
        url: 'set_coment.php',
        type: 'post',
        contentType: 'application/x-www-form-urlencoded',
        data: {'id':id,'comentario':comentario},
        beforeSend: function( xhr ) {
            $('#save__'+id).html('<img src="images/busy.gif" />');
        },
        success: function( data ){
            $('#save__'+id).html( html );
            $('#save__'+id).removeClass("btn-primary");
            $('#save__'+id).addClass("btn-success");
        }
    });
}


function show_table() {
    date_ini = $('#date_ini').val();
    date_end = $('#date_end').val();

    $('#data-info-2').dataTable({
        "bProcessing": true,
        "sAjaxSource": "get-orders-sap.php",
        "aaSorting": [[2,'desc']],
        "lengthMenu": [[10, 25, 50, 100, 150, 200], [10, 25, 50, 100, 150, 200]],
        "oLanguage": {
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            }
        },
        "fnServerParams": function ( aoData ) {
            aoData.push({"name": "date_end", "value": date_end});
            aoData.push({"name": "date_ini", "value": date_ini});
        },
        "aoColumns": [
            { mData: 'num_ped' },
            { mData: 'web_coment' },
            { mData: 'fecha' },
            { mData: 'money' },
            { mData: 'pago' },
            { mData: 'cliente' }
        ]
    });
}


$( document ).ready(function() {
    show_table();
});